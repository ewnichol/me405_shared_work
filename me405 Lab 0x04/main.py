# -*- coding: utf-8 -*-
'''
@file       main.py
@brief      Lab0x04: Hot or Not? main file
@details    This main file is used to measure two sets of data: the internal
            temperature of the microcontroller and the ambient temperature. It
            will record these measurements every 60 seconds along with the
            timestamp at which the measurements were taken. This data is then 
            appended to a CSV file called "TempData.csv". Column 1 is the time 
            stamp in seconds, column 2 is the core temperature in Celsius, and 
            column 3 is the ambient temperature of the room in Celsius. 
            
            If the sensor is not read or set up incorrectly (manufacturer ID
            is incorrect), the code will return an error message in the REPL.
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me405_shared_work/src/master/me405%20Lab%200x04/main.py
            
@author     Kyle Chuang
@author     Enoch Nicholson
@date       February 09, 2021
'''
import pyb, utime
from pyb import I2C
from mcp9808 import MCP9808

## Initializes the pyb.ADCAll object 
adc = pyb.ADCAll(12, 0x70000)

## Initializes the Nucleo as the MASTER in the I2C interface
i2c = I2C(1, I2C.MASTER, baudrate= 115200)

## Start time of the data collection
start_time = utime.ticks_ms()

## Initializes the mcp9808 driver using the I2c object
mcp = MCP9808(i2c)

## Initializes reference voltage of the adc object
adc.read_core_vref()

# Checks the manufacturer ID to verify sensor is setup
if mcp.check() == True:
    
    # Creates a csv file to append data to
    with open ('TempData.csv', 'w') as data:
        
        # Collect data until the user presses Ctrl-c
        while True:
            curr_time = utime.ticks_ms()
            time = utime.ticks_diff(curr_time, start_time)/1e3
            T_core_C = adc.read_core_temp()
            T_core_F = adc.read_core_temp()*9/5 + 32
            T_amb_C = mcp.celcius()
            T_amb_F = mcp.fahrenheit()

            print('Time: ' + str(time) +' s')
            print('Core Temp: ' + str(T_core_C) + ' C')
            print('Ambient Temp: ' + str(T_amb_C) + ' C')
            data.write('{:},{:},{:}\n'.format(time, T_core_C, T_amb_C))
            utime.sleep(60)
            
    # Prints confirmation that data is complete
    print ("The file has by now automatically been closed.")
    
# Error message if mcp.check() returns false
else:
    print('ERROR: mcp9808 temperature sensor not found!')
    


